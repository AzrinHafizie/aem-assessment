import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { resolve } from 'url';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  baseUrl = 'http://52.76.7.57:3000/api'
  token = undefined
  httpOptions

  constructor(
    private http: HttpClient,
  ) { }

  async setHeader(type){

    const httpOptions = new HttpHeaders()

    let token = JSON.parse(localStorage.getItem('token'))

    if(token){
      this.httpOptions = httpOptions.append('Authorization', 'Bearer '+token.access_token)
    }

    if(type == 'multipart'){
      this.httpOptions = httpOptions.append('Content-Type', 'multipart/form-data')
      return this.httpOptions
    } else if(type == 'json'){
      this.httpOptions = httpOptions.append('Content-Type', 'application/json')
      return this.httpOptions
    }
    
  }

}
