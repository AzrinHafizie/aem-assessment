import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { of } from 'rxjs';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigService } from './config.service';
import { resolve } from 'url';
import { reject } from 'q';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(
    private http: HttpClient,    
    public configService: ConfigService,
  ) { }

  async getDashboardData(){
    let header = await this.configService.setHeader('json')
    return new Promise((resolve, reject)=>{
      this.http.get(this.configService.baseUrl+'/dashboard', {headers: header})
        .subscribe((res:any)=>{
          let data = res
          resolve(res)
        },
        err=>{
          console.log('err', err)
          reject(err)
        })
    })
  }
}
