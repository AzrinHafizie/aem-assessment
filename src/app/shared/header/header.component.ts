import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from './../../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  loading = {
    logout: false
  }
  isLoggedIn = false

  isOpen = false

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.checkLogin()
  }

  checkLogin(){
    let token = JSON.parse(localStorage.getItem('token'))
    if(token){
      this.isLoggedIn = true
    }

    this.authService.$response
    .subscribe((res: any)=>{
      // console.log('listening', res)
      if(res.token){
        this.isLoggedIn = true
      } else if(!res.user ) {
        this.isLoggedIn = false
        this.redirectTo()
      }
    })
  }

  redirectTo(){
    this.router.navigate(['/']);
  }

  logOut(){
    this.loading.logout = true
    this.authService.logOut()
    .then(res=>{
      this.loading.logout = false
      this.redirectTo()
    })
    .catch(err=>{
      this.loading.logout = false
      console.log(err)
    })
  }

   
  toggleMenu(){
    switch (this.isOpen) {
      case true:
        this.isOpen = false
        break;
      case false:
        this.isOpen = true
        break;
    }
  }


}
